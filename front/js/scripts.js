/* IoT Viewer main script file
 * Author: Tomas Fenyk
 */
var PageHandler = (function () {

   var pageGeneratorUrl = './engine/pageGenerator.php',
       pageParams = {},
       deviceLogs = {},
       activeCharts = {},
       timeouts = [],
       measuresDisplayTypes = {};

   /* Charts operations */
   var Charts = (function () {
     return {
       /* Creates chart instance */
       createChart: function (args) {
         var measureName = args.measureName,
             type = 'line', // Default chart type is "line"
             data = args.data;

         /* Change the chart type if set */
         if (typeof args.type != 'undefined' && args.type != '') {
           type = args.type;
         } else if (typeof activeCharts[measureName] != 'undefined') {
           type = activeCharts[measureName].chart.config.type;
         }

         /* If there's more than 100 records, then edit record count to maximum of 100 records
          * so that they can be effectively presented and viewed in the chart.
          */
        var valuesCount = data.values.length,
            shortened = false,
            labelText = measureName + ' (' + data.unit + ')',
            maxValues = 100,
            styles = {
              'line' : {
                'borderColor' : '#025aa5',
                'pointBackgroundColor' : '#025aa5',
                'pointBorderColor' : '#025aa5',
                'backgroundColor' : 'transparent',
              },
              'bar' : {
                'borderColor' : '#025aa5',
                'pointBackgroundColor' : '#025aa5',
                'pointBorderColor' : '#025aa5',
                'backgroundColor' : '#4a9fe8',
              }
            };

         if (valuesCount > maxValues) {
           var selectEveryNth = Math.round(valuesCount/maxValues),
               shortenedValues = [],
               shortenedTime = [];

           shortened = true;

           for (i = 0; (i*selectEveryNth) < valuesCount; i++) {
             shortenedValues.push(data.values[i*selectEveryNth]);
             shortenedTime.push(data.time[i*selectEveryNth]);
           }

           data.time = shortenedTime;
           data.values = shortenedValues;
         }

         if (shortened) labelText += ' - shortened graph to show max 100 values';

         var chartArgs = {
           'type' : type,
           'data' : {
             'labels' : data.time,
             'datasets' : [{
               'label' : labelText,
               'data' : data.values,
               'pointHitRadius' : 15,
               'borderColor' : styles[type].borderColor,
               'pointBackgroundColor' : styles[type].pointBackgroundColor,
               'pointBorderColor' : styles[type].pointBorderColor,
               'backgroundColor' : styles[type].backgroundColor,
              }],
            },
            'options' : {
              'animation': {
                'duration': 1,
                /*'onComplete': function() {
                  var chartInstance = this.chart,
                    ctx = chartInstance.ctx;

                    ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
                    ctx.textAlign = 'center';
                    ctx.textBaseline = 'bottom';

                    this.data.datasets.forEach(function(dataset, i) {
                      var meta = chartInstance.controller.getDatasetMeta(i);
                      meta.data.forEach(function(bar, index) {
                      var data = dataset.data[index];
                      ctx.fillText(data, bar._model.x, bar._model.y - 5);
                    });
                  });
                }*/
              },
              'scales' : {
                'yAxes' : [{
                  'scaleLabel' : {
                    'display' : true,
                    'labelString' : data.unit
                  },
                  'ticks': {
                    'beginAtZero': false
                  }
                }]
              }
            }
         };

         var chartInst = new Chart($('.measure[data-measure-name="' + measureName + '"] .card-block div.display-part[data-display-type="chart"] canvas'), chartArgs); // Generate chart
         activeCharts[measureName] = {chart : chartInst, data: data};


       },

       /* Handles change of the chart display type */
       toggleChartType: function (measureName, type) {
         activeCharts[measureName].chart.destroy();
         this.createChart({type: type, data: activeCharts[measureName].data, measureName: measureName});
       },
     }
   }());

   /* Page handler object functions */
   return {
     /* First initialization */
     init: function ($) {
       /* Show loading window while ajax running */
       this.init_handlers();
       this.set_menu();
       this.set_body(pageParams, false); // False as it is not refresh (for anchor scroll)
     },

     /* Basic handlers for page elements */
     init_handlers: function () {
       this.ajax_requests_events();
       this.handle_get_parameters();
       this.action_handlers();
     },

     /* Setting menu list */
     set_menu: function () {
       var _this = this;

       $.post(pageGeneratorUrl, {getContent: 'menu'}, function(data) {
         data = _this.handle_ajax_response(data);
         $('#main-menu ul.navbar-nav').html(data.html);

         /* Initial active class set (no click evoked) */
         if (!$('#main-menu .navbar-nav li.active').length) {
           if (pageParams.page != 'undefined') {
             if (typeof pageParams.page != 'undefined') {
               var element = '#main-menu .navbar-nav li a[data-page-type=' + pageParams.page + ']';

               if (pageParams.page == 'device-data' && typeof pageParams.deviceId != 'undefined') {
                 element += '[data-device-id="' + pageParams.deviceId + '"]';
               }

               $(element).parent('li').addClass('active');
             }
           } else {
             console.log('Warning: No pageParams set for action_handlers init.');
             return false;
           }
         }
       });
     },

     /* Setting HTML content for page body */
     set_body: function (args, refresh) {
       var postArgs = {getContent: 'body', args: {}},
           _this = this;

       if (typeof refresh == 'undefined') refresh = true;

       _this.clear_timeouts();

       if (typeof args.page == 'undefined') {
         console.log('Page type not set.');
         return false;
       } else {
         if (args.page == 'device-data' && typeof args.deviceId == 'undefined') {
           console.log('Device id not set.');
           return false;
         } else {
           postArgs.args = args;
         }

         if (args.type == 'dashboard') {
           postArgs.page = args.page;
         }
       }

       $.post(pageGeneratorUrl, postArgs, function(data) {
         data = _this.handle_ajax_response(data);

         $('#main-content').html(data.html); // Apply content HTMl and then fetch the data

         if (args.page == 'device-data') { // Handler after page was printed (charts, tables js)
           /* Handle display type for measures if the body content was reloaded */
           $.each(measuresDisplayTypes, function(measureName, displayType) {
             var measureDiv = '.measure[data-measure-name="' + measureName + '"]';

             $(measureDiv + ' .card .card-block div').hide();
             $(measureDiv + ' .card .card-header .nav li.nav-item a').removeClass('active');
             $(measureDiv + ' .card .card-header li a[data-display-type="' + displayType + '"]').addClass('active');
             $(measureDiv + ' .card .card-block div[data-display-type="' + displayType + '"]').show();

             /* Reselect chart types if changed from the previous session */
             if (typeof activeCharts[measureName] != 'undefined') {
               $(measureDiv + ' .card .card-block .display-part[data-display-type="chart"] form select[name="chartType"]').val(activeCharts[measureName].chart.config.type);
             }
           });

           /* Render charts view */
           $.each(data.chartData, function(measureName, v) {
             /* Create chart for each of the measures */
             Charts.createChart({measureName: measureName, data: v});

             /* Datepicker */
             if ($('.datepicker').length) {
               $('.datepicker').datepicker();
             }

             /* Set the timeout to refresh page for fresh data */
             var newDeviceDataTimeout = setTimeout(function() {
               _this.set_body(pageParams);
             }, 1000*60*10);

             timeouts.push(newDeviceDataTimeout);
           });
         }

         /* Navigate to anchor if set on page load */
         var anchorParams = window.location.href.split('#')[1],
             anchorObj = $('#device-data-content .measure#' + anchorParams);

         if (anchorParams != '' && !refresh && anchorObj.length) {
           $('html, body').animate({
               scrollTop: $(anchorObj).offset().top
           }, 500);
         }
       });
     },

     /* Clear timeouts */
     clear_timeouts : function () {
       $.each(timeouts, function(i, v) {
         clearTimeout(v);
       });
     },

     /* All menu action handlers and listeners */
     action_handlers: function () {
        var _this = this;

        /* Device selection in the left menu */
        $(document).on('click', '#main-menu .navbar-nav li a', function (e) {
          e.preventDefault();

          $('#main-menu .navbar-nav li').removeClass('active');
          $(this).parent('li').addClass('active');

          history.pushState(null, null, $(this).attr('href')); // Pass change to URL

          _this.handle_get_parameters(); // Sets pageParams variable
          _this.set_body(pageParams);
       });

       /* Measure display type */
       $(document).on('click', '.measure .card .card-header .nav li.nav-item a', function (e) {
         e.preventDefault();
         var measureName = $(this).parents('div.measure').attr('data-measure-name'),
             measureDiv = '.measure[data-measure-name="' + measureName + '"]';

         $(measureDiv + ' .card .card-header .nav li.nav-item a').removeClass('active');
         $(this).addClass('active');
         $(measureDiv + ' .card .card-block div').hide();
         $(measureDiv + ' .card .card-block div[data-display-type="' + $(this).attr('data-display-type') + '"]').show();

         measuresDisplayTypes[measureName] = $(this).attr('data-display-type');
      });

      /* Scroll when measure selected */
      $(document).on('click', '#device-measurements-menu a', function (e) {
        e.preventDefault();
        var anchorObj = $('#device-data-content .measure' + $(this).attr('href'));

        history.pushState(null, null, $(this).attr('href')); // Pass change to URL

        if (anchorObj.length) {
          $('html, body').animate({
              scrollTop: $(anchorObj).offset().top
          }, 500);
        }
      });

      /* Change chart type on selection */
      $(document).on('change', 'form select[name="chartType"]', function(e){
         e.preventDefault();
         Charts.toggleChartType($(this).parents('.measure').attr('data-measure-name'), $(this).val());
       });

      /* Display logs for a selected time range */
      $(document).on('click', 'button[name="changeTimeParams"]', function (e) {
        e.preventDefault();

        pageParams.fromDate = $('input[name="fromDate"]').val();
        pageParams.toDate = $('input[name="toDate"]').val();

        if ($('select[name="timeRange"]').val() != 'default') {
          pageParams.timeRange = $('select[name="timeRange"]').val();
        }

        _this.set_body(pageParams);
      });
     },

     /* Checks GET and other URL parameters and sets appropriate variables for further use */
     handle_get_parameters: function () {
       var getParams = window.location.href.split('#')[0].split('?')[1];
       if (typeof getParams != 'undefined') {
         $.each(getParams.split('&'), function (i, param) {
           if (typeof param.split('=')[1] != 'undefined' && param.split('=')[1].length > 0) {
             var paramName = param.split('=')[0],
                 paramValue = param.split('=')[1];

             switch (paramName) {
               case 'showData':
                  pageParams.page = 'device-data';
                  pageParams.deviceId = paramValue;
               break;
             }
           }
         });
      } else {
        pageParams.page = 'dashboard';
      }
     },

     handle_ajax_response: function (reponse) {
      try {
        response = JSON.parse(reponse);

        if (typeof response.status != 'undefined') {
          if (response.status) {
            return response;
          } else {
            console.log(response.message);
          }
        }
      } catch (er) {
        $('body #main-content').append(reponse);
        console.log(reponse);
      }
     },

     /* Handler for displaying loading windon on ajax events */
     ajax_requests_events: function () {
       $(document).ajaxStart(function() {
         $('body').append('<div id="waiting-window">Loading content</button></div>');
       });

       $(document).ajaxStop(function() {
         $('#waiting-window').remove();
       });
     },
  };
}(jQuery));

$(document).ready(function() {
  PageHandler.init(); // Start the page
});
