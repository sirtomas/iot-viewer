<?php
  /* Handles displayed page data. */
  class PageGenerator {

    private $dbManager;
    private $defaultTimeRange = 10080; // Default time range in minutes (10080 = 1 week) for log queries

    function __construct () {
      require_once 'dbManager.php';
      $this->dbManager = new DbManager();
    }

    /* Generates HTML list of devices. */
    public function generate_menu_items () {
      $html = '';
      $devicesQuery = $this->dbManager->query("SELECT device_name, id AS device_id, device_icon, device_ident, description FROM devices");
      $currentDevice = null;
      $devices = array();

      /* Parse data */
      foreach ($devicesQuery as $r) {
        $devices[$r['device_id']]['device_ident'] = $r['device_ident'];
        $devices[$r['device_id']]['device_name'] = $r['device_name'];
        $devices[$r['device_id']]['description'] = $r['description'];
        $devices[$r['device_id']]['device_icon'] = $r['device_icon'];
      }

      /* Build menu HTML */
      $html .= '<li class="nav-item"><a href="./" data-page-type="dashboard" class="nav-link"><span class="glyphicon glyphicon-home"></span>Dashboard</a></li>'; // Add home link

      foreach ($devices as $deviceKey => $deviceValue) {
        $html .= '<li class="nav-item"><a href="?showData=' . $deviceKey . '" class="nav-link" data-page-type="device-data" data-device-id="' . $deviceKey . '" title="' . $deviceValue['device_ident'] . ' - ' . $deviceValue['description'] . '"><span class="' . $deviceValue['device_icon'] . '"></span>' . $deviceValue['device_name'] . '</a></li>';
        $currentDevice = $deviceKey;
      }

      return $html;
    }

    /* Returns basic device data by ID */
    private function get_device_info_by_id ($deviceId) {
      $result = $this->dbManager->query('SELECT device_name, device_ident, description, device_icon FROM devices LEFT JOIN locations ON locations.id = devices.location WHERE devices.id LIKE ? LIMIT 1', array($deviceId));

      if (count($result)) {
        return $result[0];
      } else {
        return false;
      }
    }

    /* Function retuns device measured data between specified dates */
    public function get_device_logs ($deviceId, $args = null) {
      $logsStmt = 'SELECT log_time, log_value, measure_name, unit FROM logs JOIN measures ON measures.id = logs.measure JOIN measure_units ON measures.measure_unit = measure_units.id WHERE device LIKE ?';
      $queryArgs = array($deviceId);

      /* Time range */
      if (isset($args['fromDate']) && isset($args['toDate'])) {
        $logsStmt .= ' AND log_time BETWEEN ? AND ?';
        array_push($queryArgs, date('Y-m-d H:i:s', strtotime($args['fromDate'])), date('Y-m-d H:i:s', strtotime($args['toDate'])));
      }

      //var_dump($queryArgs);
      //var_dump($logsStmt);

      $logsQuery = $this->dbManager->query($logsStmt, $queryArgs);

      return $logsQuery;
    }

    /* Generates body HTML and data that can be displayed. Page types are specified by switch. */
    public function generate_body_data ($name, $args = null) {
      $bodyData = array(); // Return value
      $html = ''; // Printed HTML

      switch ($name) {
          case 'dashboard':

            $pageData = array('statistics' => $this->get_statistics());
            ob_start();
            include '../templates/dashboard.php';
            $html .= ob_get_clean();
            break;

          case 'device-data':
            if (isset($args['deviceId'])) {
              $pageData = array('fromDateInput' => '', 'toDateInput' => '');
              $shifted = false;

              /* Get device statistics data */
              $statsData = $this->get_statistics(array('deviceId' => $args['deviceId']));

              if ($statsData === false) return false; // Something went wrong

              /* Pass input values to the template */
              if (isset($args['fromDate'])) {
                $pageData['fromDateInput'] = $args['fromDate'];
              }

              if (isset($args['toDate'])) {
                $pageData['toDateInput'] = $args['toDate'];
              }

              /* Process logs if there are any */
              if ($statsData['log_quantity']) {
                /* Check if time range is not set */
                if (isset($args['timeRange'])) {
                  $args['toDate'] = date('m/d/Y H:i:s', strtotime($statsData['last_log_time']));
                  $args['fromDate'] = date('m/d/Y H:i:s', strtotime('-' . $args['timeRange'] . ' minute', strtotime($args['toDate']))); // Time range has to be in minutes
                  $pageData['timeRangeInput'] = $args['timeRange'];

                  /* Time range has precedence over input dates */
                  $pageData['toDateInput'] = '';
                  $pageData['fromDateInput'] = '';
                } else {
                  $pageData['timeRangeInput'] = $this->defaultTimeRange; // Set default time range if nothing sent
                }

                /* If no time border was set, display latest logs within default timerange */
                if ((!isset($args['toDate']) || $args['toDate'] == '') &&
                    (!isset($args['fromDate']) || $args['fromDate'] == ''))
                {
                  $args['toDate'] = date('m/d/Y H:i:s', strtotime($statsData['last_log_time']));
                }

                /* Set the other end for time range (if not set) */
                if (!isset($args['fromDate'])) {
                  $args['fromDate'] = date('m/d/Y H:i:s', strtotime('-' . $this->defaultTimeRange . ' minute', strtotime($args['toDate'])));
                }

                if (!isset($args['toDate'])) {
                  $args['toDate'] = date('m/d/Y H:i:s', strtotime('+' . $this->defaultTimeRange . ' minute', strtotime($args['fromDate'])));
                }

                /* Get log data */
                $logData = $this->get_device_logs($args['deviceId'], $args); // Default time range query or optional parameters

                if ($logData === false) return false; // Something went wrong

                /* Processed log data */
                if (count($logData)) {
                  $pageData['measurements'] = $this->generate_device_logs_template_data($logData);
                }
              }

              /* Pre-set time ranges for log filter */
              $pageData['timeRangesSelection'] = array('default' => 'Select time range', '43200' => '30 days', '20160' => '14 days', '10080' => '7 days', '1440' => '1 day', '60' => '1 hour');

              /* Pass data for charts to return value */
              if (isset($pageData['measurements'])) {
                $bodyData['chartData'] = $pageData['measurements']['chartData'];
              } else {
                $bodyData['chartData'] = array();
              }

              /* Prepare page data for template */
              $pageData['deviceInfo'] = $this->get_device_info_by_id($args['deviceId']);
              $pageData['statistics'] = $statsData;

              ob_start();
              include '../templates/device-data.php';
              $html .= ob_get_clean();

            } else {
              $html = 'Error: Device id not defined.';
            }

            break;

          default:
            $html = 'Error: Defined page "' . $name . '" was not found.';
            return false;
      }

      $bodyData['html'] = $html;

      return $bodyData;
    }

    /* Generates necessarry HTML and calculation for the device-data page template. */
    private function generate_device_logs_template_data ($allLogs) {
      $allLogsCount = 0;
      $firstLogTime = null;
      $lastLogTime = null;
      $measures = array(); // Measures information
      $chartData = array();

      /* Parse all the log data and retrieve what is needed */
      foreach ($allLogs as $log) {
        $measureName = $log['measure_name'];
        $logVal = $log['log_value'];
        $logUnit = $log['unit'];
        $logTime = date('m/d/Y H:i:s', strtotime($log['log_time'])); // Time can be retrieved from DB from "log_time" column (time set on insert), or indenpendently for each log measurement

        /* First and last measurement time */
        if ($firstLogTime == null || strtotime($logTime) < strtotime($firstLogTime)) {
          $firstLogTime = $logTime;
        }

        if ($lastLogTime == null || strtotime($logTime) > strtotime($lastLogTime)) {
          $lastLogTime = $logTime;
        }

        /* Measures iteration */
        if (!array_key_exists($measureName, $measures)) {
          /* Charts info and data */
          $chartData[$measureName]['values'] = array();
          $chartData[$measureName]['time'] = array();
          $chartData[$measureName]['unit'] = $logUnit;

          /* Measures info used for quick overview in the template */
          $measures[$measureName]['logsCount'] = 0;
          $measures[$measureName]['maxValue'] = null;
          $measures[$measureName]['minValue'] = null;
          $measures[$measureName]['mean'] = null;
          $measures[$measureName]['lastValue'] = null;
          $measures[$measureName]['lastValueTime'] = null;
        }

        /* Add values for chart */
        $chartData[$measureName]['values'][] = $logVal;
        $chartData[$measureName]['time'][] = $logTime;

        /* Max value stat */
        if ($measures[$measureName]['maxValue'] == null || $measures[$measureName]['maxValue'] < $logVal) {
          $measures[$measureName]['maxValue'] = $logVal;
        }

        /* Min value stat */
        if ($measures[$measureName]['minValue'] == null || $measures[$measureName]['minValue'] > $logVal) {
          $measures[$measureName]['minValue'] = $logVal;
        }

        /* Last value time */
        if (strtotime($measures[$measureName]['lastValueTime']) < strtotime($logTime) || is_null($measures[$measureName]['lastValueTime'])) {
          $measures[$measureName]['lastValueTime'] = $logTime;
        }

        /* Mean stat calculation */
        if (!is_null($measures[$measureName]['mean'])) {
          $measures[$measureName]['mean'] = round(($measures[$measureName]['mean'] + $logVal)/2, 2);
        } else {
          $measures[$measureName]['mean'] = round($logVal, 2);
        }

        /* Last value stats */
        $measures[$measureName]['lastValue'] = $logVal;
        $measures[$measureName]['logsCount']++; // Count measurement for current measure
        $allLogsCount++;
      }

      /* Return data for page template */
      return array('allLogsCount' => $allLogsCount, 'measuresCount' => count($measures),
                   'firstLogTime' => date('m/d/Y H:i:s', strtotime($firstLogTime)),
                   'lastLogTime' => date('m/d/Y H:i:s', strtotime($lastLogTime)),
                   'chartData' => $chartData, 'measures' => $measures);
    }

    /* Returns basic log statistics for specified time range and device */
    private function get_statistics ($args = null) {
      $queryArgs = array();

      $stmt = 'SELECT count(log_time) AS log_quantity, max(log_time) AS last_log_time, min(log_time) AS first_log_time';

      if (!isset($args['deviceId'])) {
        $stmt .= ', COUNT(DISTINCT device) AS devices_quantity';
      }

      $stmt .= ' FROM logs';

      if (isset($args['fromDate']) && isset($args['toDate']) || isset($args['deviceId'])) {
        $stmt .= ' WHERE';

        /* Time range */
        if (isset($args['fromDate']) && isset($args['toDate'])) {
          $stmt .= ' log_time BETWEEN ? AND ?';
          array_push($queryArgs, $args['fromDate'], $args['fromDate']);
        }

        /* Connect conditions */
        if (isset($args['fromDate']) && isset($args['toDate']) && isset($args['deviceId'])) $stmt .= ' AND';

        /* Device ID */
        if (isset($args['deviceId'])) {
          $stmt .= ' device LIKE ?';
          array_push($queryArgs, $args['deviceId']);
        }
      }

      $result = $this->dbManager->query($stmt, $queryArgs)[0];

      return $result;
    }
  }

  /* AJAX handling (only POST calls are currently used) */
  if (isset($_POST)) {
    if (isset($_POST['getContent'])) {
      $pageGenerator = new PageGenerator();

      if ($_POST['getContent'] == 'menu') {
        echo json_encode(array('status' => true, 'html' => $pageGenerator->generate_menu_items()));
        return true;
      } else if ($_POST['getContent'] == 'body') {
        $bodyData = null;
        $additionalArgs = null;

        if (isset($_POST['args'])) $additionalArgs = $_POST['args'];

        $pageName = $additionalArgs['page'];

        if (!isset($additionalArgs['page'])) {
          echo json_encode(array('status' => false, 'message' => 'Warning: Page not set.'));
          return false;
        }

        if ($pageName == 'device-data') {
          $deviceId = $additionalArgs['deviceId'];

          if (isset($deviceId)) {
            $bodyData = $pageGenerator->generate_body_data($pageName, $additionalArgs);

            if ($bodyData === false) {
              echo json_encode(array('status' => false, 'message' => 'Error: Could not retrive device data.'));
              return false;
            } else {
              echo json_encode(array('status' => true, 'html' => $bodyData['html'], 'chartData' => $bodyData['chartData']));
              return true;
            }
          } else {
            echo json_encode(array('status' => false, 'message' => 'Warning: Device id not set.'));
            return false;
          }
        } else if ($pageName == 'dashboard') {
          $bodyData = $pageGenerator->generate_body_data($pageName, $additionalArgs);
          echo json_encode(array('status' => true, 'html' => $bodyData['html']));
          return true;
        }
      } else {
        echo json_encode(array('status' => false, 'message' => 'Warning: Wrong "get_content" parameter value defined.'));
        return false;
      }
    }
  }
?>
