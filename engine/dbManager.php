<?php
  class DbManager {

    private static $server = '';
    private static $user = '';
    private static $password = '';
    private static $database = '';
    private $conn;

    function __construct () {
      $this->conn = $this->connect(self::$server, self::$user, self::$password, self::$database);
    }

    function connect ($server, $user, $password, $database) {
      try {
        $conn = new PDO('mysql:host=' . $server . ';dbname=' . $database, $user, $password);
        $conn->query('SET NAMES utf8');
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        return $conn;
      } catch (PDOException $e) {
        echo 'Cannot connect to the database: ' . $e->getMessage();
        die();
      }
    }

    function lastInsertId () {
      return $this->conn->lastInsertId();
    }

    function query ($request, $args = null) {
      try {
        if ($args == null) { // Classic simple query
          $result = $this->conn->query($request)->fetchAll(PDO::FETCH_ASSOC);
          return $result;
        } else { // Prepared statement query because of the input parameters
          $query = $this->conn->prepare($request);
          $queryResult = $query->execute($args);

          if (preg_match("/SELECT/", $request)) {
            $result = $query->fetchAll(PDO::FETCH_ASSOC);
          } else {
            $result = $queryResult;
          }

          return $result;
        }
      } catch (PDOException $e) {
        echo 'An error occurred while performing query: ' . $e->getMessage();
        return false;
      }
    }
  }
  ?>
