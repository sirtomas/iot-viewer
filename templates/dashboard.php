<h2 class="page-header">Dashboard</h2>

<?php if ($pageData['statistics']['log_quantity']): ?>
<div class="row">
  <div class="col">
    <div class="card mb-2">
      <div class="card-block">
        <h3 class="card-title">
          <span class="stat-value"><?php echo $pageData['statistics']['log_quantity'] ?>/<?php echo $pageData['statistics']['devices_quantity'] ?></span>
        </h3>
        <p class="card-text">Total number of logs/devices with logs</p>
      </div>
    </div>
  </div>
  <div class="col-sm-8">
    <div class="card mb-2">
      <div class="card-block">
        <h3 class="card-title">
          <span class="stat-value"><?php echo date('m/d/Y H:i:s', strtotime($pageData['statistics']['first_log_time'])) ?> - <?php echo date('m/d/Y H:i:s', strtotime($pageData['statistics']['last_log_time'])) ?></span>
        </h3>
        <p class="card-text">Data coverage in time</p>
      </div>
    </div>
  </div>
</div>
<?php else: ?>
<div class="row">
  <div class="col">
    <?php if ($pageData['statistics']['devices_quantity']): ?>
    <div class="alert alert-warning" role="alert">
      No logs available for any of the devices.
    </div>
    <?php else: ?>
    <div class="alert alert-warning" role="alert">
      No devices with logs available.
    </div>
    <?php endif; ?>
  </div>
</div>
<?php endif; ?>

<div class="row mt-3">
  <div class="col">
    <h2 class="mb-3">Welcome</h2>
    <p><strong>Welcome to the IoT Viewer application - a simple IoT log data viewer!</strong> If you want to view data for a particular device, just select it in the left menu. For every device you can select a type of log data view (chart, table,...) depending on the data type.</p>
  </div>
</div>
