<h2 class="page-header"><span class="device-name"><?php echo $pageData['deviceInfo']['device_name']; ?></span><small class="text-muted"> - device data</small></h2>

<?php if ($pageData['statistics']['log_quantity']) : ?>
<div class="row mb-4">
  <div class="col">
    <div class="card mb-1">
      <div class="card-block">
        <h3 class="card-title">
          <span class="stat-value"><?php echo $pageData['statistics']['log_quantity'] ?></span>
        </h3>
        <p class="card-text">Total number of logs for this device</p>
      </div>
    </div>
  </div>
  <div class="col-sm-8">
    <div class="card mb-1">
      <div class="card-block">
        <h3 class="card-title">
          <span class="stat-value"><?php echo date('m/d/Y H:i:s', strtotime($pageData['statistics']['first_log_time'])) ?> - <?php echo date('m/d/Y H:i:s', strtotime($pageData['statistics']['last_log_time'])) ?></span>
        </h3>
        <p class="card-text">Data coverage in time</p>
      </div>
    </div>
  </div>
</div>
<?php endif; ?>

<?php if ($pageData['statistics']['log_quantity']) : ?>
<div class="row mb-4">
  <div class="col-md">
    <form class="form-inline">
      <label class="mr-sm-2" for="log-time-range-select">Select logs from</label>
      <input type="text" class="mb-2 mr-sm-2 mb-sm-0 form-control" value="<?php echo $pageData['fromDateInput']; ?>" placeholder="MM/DD/YYYY hh:mm:ss" name="fromDate">

      <label class="mr-sm-2" for="toDate">to</label>
      <input type="text" class="mb-2 mr-sm-2 mb-sm-0 form-control" value="<?php echo $pageData['toDateInput']; ?>" placeholder="MM/DD/YYYY hh:mm:ss" name="toDate">

      <label class="mr-sm-2" for="timeRange">or</label>
      <select class="custom-select mb-2 mr-sm-2 mb-sm-0 form-control" name="timeRange">
        <?php foreach ($pageData['timeRangesSelection'] as $interval => $title) : ?>
          <option value="<?php echo $interval ?>" <?php if(isset($pageData['timeRangeInput']) && ($pageData['timeRangeInput'] == $interval)) echo 'selected'; ?>><?php echo $title ?></option>
        <?php endforeach; ?>
      </select>

      <label class="mr-sm-2">from last log</label>
      <button type="button" class="btn btn-primary mb-2 mr-sm-2 mb-sm-0 form-control" name="changeTimeParams">Submit</button>
    </form>
  </div>
</div>
<?php endif; ?>

<div class="row" id="device-time-menu">
  <div class="col-md col-sm-12 text-left align-left" id="log-time-range-text">
    <?php if (isset($pageData['measurements'])): ?>
      <h3>Measured data</h3>
      <hr />
      <ul>
        <li><strong><?php echo $pageData['measurements']['allLogsCount']; ?></strong> measurement(s) from <strong><?php echo $pageData['measurements']['firstLogTime']; ?></strong> to <strong><?php echo $pageData['measurements']['lastLogTime']; ?></strong></li>
      </ul>
    <?php elseif ($pageData['statistics']['log_quantity']): ?>
      <div class="alert alert-warning" role="alert">
        No logs available for this time period.
      </div>
    <?php else: ?>
      <div class="alert alert-warning" role="alert">
        No logs available for this device.
      </div>
    <?php endif; ?>
  </div>
</div>

<?php if (isset($pageData['measurements'])): ?>
  <div class="row" id="device-measurements-menu">
    <div class="col-12 col-sm-auto my-3">
      <h4 class="measure-title">Metrics (<?php echo $pageData['measurements']['measuresCount']; ?>)</h4>

      <?php foreach ($pageData['measurements']['measures'] as $measureName => $measureStatsData) : ?>
        <a href="#<?php echo md5($measureName); ?>" class="list-group-item list-group-item-action justify-content-between"><?php echo $measureName; ?> <div class="align-right"><span class="badge badge-primary badge-pill"><? echo $measureStatsData['lastValue'] . ' ' . $pageData['measurements']['chartData'][$measureName]['unit'] ?></span><span class="badge badge-default badge-pill"><?php echo $measureStatsData['logsCount']; ?></span></div></a>
      <?php endforeach; ?>
    </div>
  </div>

  <div class="row" id="device-data-content">
    <div class="col-sm-12">
      <?php foreach ($pageData['measurements']['measures'] as $measureName => $measureStatsData) : ?>
        <div class="measure" id="<?php echo md5($measureName); ?>" data-measure-name="<?php echo $measureName; ?>">
          <h3 class="measure-title"><?php echo $measureName; ?> <small class="text-muted log-count">(<?php echo $measureStatsData['logsCount']; ?>)</small></h3>
          <div class="card">
            <div class="card-header">
              <ul class="nav nav-tabs card-header-tabs">
                <li class="nav-item">
                  <a class="nav-link active" href="#" data-display-type="overview">Overview</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#" data-display-type="chart">Chart</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#" data-display-type="table">Table</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#" data-display-type="json">JSON</a>
                </li>
              </ul>
            </div>
            <div class="card-block">
              <div class="display-part active" data-display-type="chart">
                <form class="form-inline">
                  <label class="sm-2 mr-2" for="chartType">Change chart type</label>
                  <select class="custom-select mb-2 mr-sm-2 mb-sm-0" name="chartType">
                    <option value="line" selected>Line</option>
                    <option value="bar">Bar</option>
                  </select>
                </form>
                <canvas class="chart"></canvas>
              </div>
              <div class="display-part" data-display-type="table">
                <table class="table">
                  <thead>
                    <tr>
                      <th>Value (<?php echo $pageData['measurements']['chartData'][$measureName]['unit'] ?>)</th>
                      <th>Datetime</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach ($pageData['measurements']['chartData'][$measureName]['values'] as $measurementIndex => $measurementVal) : ?>
                      <tr>
                          <td><?php echo $measurementVal ?></td>
                          <td><?php echo $pageData['measurements']['chartData'][$measureName]['time'][$measurementIndex]; ?></td>
                      </tr>
                    <?php endforeach; ?>
                  </tbody>
                </table>
              </div>
              <div class="display-part" data-display-type="json">
                <pre class="pre-scrollable"><code><?php echo '"' . $measureName . '" : ' . json_encode($pageData['measurements']['chartData'][$measureName], JSON_PRETTY_PRINT); ?></code></pre>
              </div>
              <div class="display-part" data-display-type="overview">
                <table class="table">
                  <thead>
                    <tr>
                      <th>Stat</th>
                      <th>Value</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                        <td>Last value</td>
                      <td><?php echo $measureStatsData['lastValue'] . ' ' . $pageData['measurements']['chartData'][$measureName]['unit'] ?>, <?php echo $measureStatsData['lastValueTime'] ?></td>
                    </tr>
                    <tr>
                      <td>Mean</td>
                      <td><?php echo $measureStatsData['mean'] . ' ' . $pageData['measurements']['chartData'][$measureName]['unit'] ?></td>
                    </tr>
                    <tr>
                      <td>Min value</td>
                      <td><?php echo $measureStatsData['minValue'] . ' ' . $pageData['measurements']['chartData'][$measureName]['unit'] ?></td>
                    </tr>
                    <tr>
                      <td>Max value</td>
                      <td><?php echo $measureStatsData['maxValue'] . ' ' . $pageData['measurements']['chartData'][$measureName]['unit'] ?></td>
                    </tr>
                    <tr>
                      <td>Number of measurements</td>
                      <td><?php echo $measureStatsData['logsCount']; ?></td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      <?php endforeach; ?>
    </div>
</div>
<?php endif; ?>
