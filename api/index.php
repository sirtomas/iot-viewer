<?php

  /* APIManager class handle API calls.

   Sample data JSON: {"some_measure_name": {
                        "values" : [25, 45],
                        "time" : [2017-05-05 05:05:05, 2017-05-05 05:05:06],
                        "unit": "measure_unit"
                      },
                      "another_measure": {
                        "values" : [456, 123],
                        "unit": "another_measure_unit"
                      },
                    }

    "values" - mandatory and represents measured values
    "time" - optional - the time is bound to the value by array index.
             If no time is set, the default timestamp from DB is used.
    "unit" - mandatory parameter that represents unit for the measure
           - unit should be same for all future logs of this measure
 */

  class APIManager {

    private $conn;

    function __construct () {
      require_once('../engine/dbManager.php');
      $this->conn = new DbManager();
      $this->parse_input_data();
    }

    function parse_input_data () {
      $method = $_SERVER['REQUEST_METHOD'];

      if ($method == 'POST') { // Only POST supported
        $inputData = $_POST;

        var_dump($_POST);

        /* Check if all POST data for the log are present, currently only one action "addLog" is defined */
        if ($inputData['ident'] && $inputData['ident'] != '' &&
            $inputData['key']  && $inputData['key'] != '' &&
            $inputData['data'] && $inputData['data'] != '') {

            $deviceId = $this->verify_device($inputData['ident'], $inputData['key']); // Check device credentials

            if ($deviceId !== false) { // Device credentials OK
              $dataJson = json_decode($this->sanitize_data($inputData['data']), true);

              /* Validate JSON format */
              if (json_last_error() == JSON_ERROR_NONE) {
                $insert = $this->add_log($deviceId, $dataJson);

                if ($insert) {
                  $this->return_status(200, "Log successfully inserted to the database.");
                } else {
                  $this->return_status(500, "Error on the server, please contact the administrator.");
                }
              } else {
                $this->return_status(400, "Recieved message is not in a valid JSON format.");
              }
            } else {
              $this->return_status(401, "Wrong credentials.");
            }
          } else {
            $this->return_status(400, "Some of the JSON parameters is missing.");
          }
      } else {
        $this->return_status(400, "This method is not supported.");
      }
    }

    /* Checks if the device is registered */
    function verify_device ($deviceIdent, $deviceKey) {
      $deviceData = $this->conn->query('SELECT id FROM devices WHERE device_ident LIKE ? AND device_key LIKE ? LIMIT 1', array($deviceIdent, $deviceKey));

      if (count($deviceData)) {
        return $deviceData[0]['id'];
      } else {
        return false;
      }
    }

    /* Function to insert log data into database */
    function add_log ($deviceId, $logData) {
      /* Parse measures */
      foreach ($logData as $measureName => $measureData) {
        $unitName = null;

        if ($measureName == '' || !isset($measureData['values'])) return false;
        if (!is_null($measureData['unit']) && $measureData['unit'] != '') $unitName = $measureData['unit'];

        /* Check measure and unit presence in DB */
        $measureId = $this->verify_measure($measureName, $measureData['unit']);

        /* Add measure to the database if it does not exists */
        if (!$measureId) {
          $addMeasure = $this->add_measure($measureName, $unitName);

          if ($addMeasure) $measureId = $this->conn->lastInsertId();
        }

        /* Insert each value from log measurements */
        for ($i = 0; $i < count($measureData['values']); $i++) {
          $insertDate = date("Y-m-d H:i:s", time());

          if (isset($measureData['time'])) $insertDate = date("Y-m-d H:i:s", $measureData['time']);
          var_dump($measureData['values'][$i]);
          $insertLog = $this->conn->query('INSERT INTO logs (device, log_value, measure, log_time) VALUES (?, ?, ?, ?)', array($deviceId, $measureData['values'][$i], $measureId, $insertDate));

          if (!$insertLog) {
            return false;
          }
        }
      }

      return true;
    }

    /* Very basic data sanitisation */
    function sanitize_data($inputData) {
      return strip_tags($inputData);
    }

    /* Function to return page with response code */
    function return_status ($code, $message = null) {
      $responseHeader = 'HTTP/1.1 ' . $code . ' ';
      $responseMessage = '';
      $responseTitle = '';
      $status = true;

      if (!is_null($message)) $responseMessage = $message;

      switch ($code) {
          case '200':
              $responseTitle = 'OK';
              break;
          case '500':
              $responseTitle = 'Internal Server Error';
              $status = false;
              break;
          case '401':
              $responseTitle = 'Unauthorized';
              $status = false;
              break;
          case '400':
              $responseTitle = 'Bad Request';
              $status = false;
              break;
          case '406':
              $responseTitle = 'Not Acceptable';
              $status = false;
              break;
      }

      header($responseHeader);
      echo '{"status":"' . $status . '", "code" : "' . $code . '", "response" : "' . $responseTitle . '", "message" : "' . $responseMessage . '"}';
      exit();
    }

    /* Verifies measure existence */
    function verify_measure ($name, $unitName = null) {
      $measureExists = $this->conn->query('SELECT measure_units.unit, measures.id AS measureId, measures.measure_name FROM measures JOIN measure_units ON measure_units.id = measures.measure_unit WHERE measures.measure_name LIKE ? LIMIT 1', array($name));

      if (count($measureExists) && $name == $measureExists[0]['measure_name'] && $unitName === $measureExists[0]['unit']) {
        return $measureExists[0]['measureId'];
      } else {
        return false;
      }
    }

    /* Add measure to database */
    function add_measure ($name, $unitName = null) {
      $unitId = null;

      /* Check if unit exists */
      if (!is_null($unitName)) {
        $unitExists = $this->conn->query('SELECT * FROM measure_units WHERE unit = ? LIMIT 1', array($unitName));

        if (count($unitExists)) {
          $unitId = $unitExists[0]['id'];
        } else {
          $unitId = $this->add_unit($unitName);
        }
      }

      /* Add the measure */
      $addUnit = $this->conn->query('INSERT INTO measures (measure_name, measure_unit) VALUES (?, ?)', array($name, $unitId));

      if ($addUnit) {
        return $this->conn->lastInsertId();
      } else {
        return false;
      }
    }

    /* Add unit to database */
    function add_unit ($unitName) {
      $addUnit = $this->conn->query('INSERT INTO measure_units (unit) VALUES (?)', array($unitName));

      if ($addUnit) {
        return $this->conn->lastInsertId();
      } else {
        return false;
      }
    }
  }

  $APIManager = new APIManager(); // Create instance
?>
